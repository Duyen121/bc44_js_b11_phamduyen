//input: regtangleWidth, regtangleLength
var regtangleWidth = 10;
var regtangleLength = 16;

//progress
var regtangleCircumference = (regtangleLength + regtangleWidth) * 2;
var regtangleSuperficies = regtangleLength * regtangleWidth;

//output: regtangleCircumference, regtangleSuperficies

//console output
console.log('Circumference of the regtangle: ', regtangleCircumference);
console.log('Superficies of the regtangle: ', regtangleSuperficies);
