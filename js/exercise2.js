// input: 5 real numbers
var a, b, c, d, e;
a = 1.5;
b = 2;
c = -3;
d = 4.5;
e = 0.75;

// progress
var arithmeticMean = (a + b + c + d + e) / 5;

//output: arithmeticMean

// console output
console.log('Arithmetic mean: ', arithmeticMean);
