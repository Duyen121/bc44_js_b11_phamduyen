//input: a random 2-digit number (twoDigitNumber)
var twoDigitNumber = 25;

//progress
var tens = Math.floor(twoDigitNumber / 10);
var units = twoDigitNumber % 10;

var sumOfTensAndUnits = tens + units;

//output: sumOfTensAndUnits

//console output
console.log('Sum of Tens and Units of that two-digit number: ', sumOfTensAndUnits);

